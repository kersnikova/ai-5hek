# AI 5hek

Python koda za sklop 10 delavnic na temo umetne inteligence.

Potrebne inšatalcije:
- Python 3
- Jupyter Notebook
- Open CV (cv2)

Koda je razdeljena v pet map oz. pet podsklopov:
1. Python osnove
2. Strojno učenje
3. Računalniški vid
4. Nevronske mreže
5. Prepoznava obrazov
